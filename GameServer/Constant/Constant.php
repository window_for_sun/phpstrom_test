<?php
/**
 * Created by PhpStorm.
 * User: 이창선
 * Date: 2018-01-16
 * Time: 오후 3:38
 */

namespace GameServer\Constant;

define('DATABASE_HOST', '');
define('DATABASE_NAME', '');
define('DATABASE_PASSWORD', '');
define('DATABASE_CHARSET', '');
defile('DATABASE_USERNAME', '');

define('MEMCACHE_HOST', 'localhost');
define('MEMCACHE_PORT', '11211');
define('MEMCACHE_TIMEOUT', '1000');

define('REDIS_HOST', 'localhost');
define('REDIS_PORT', '6379');
