<?php
/**
 * Created by PhpStorm.
 * User: 이창선
 * Date: 2018-01-16
 * Time: 오후 3:28
 */

namespace GameServer\Config;

use GameServer\Config\MemcacheServerInfo;
use GameServer\Config\RedisServerInfo;
use GameServer\Constant;

class ConfigInfo
{
//    private $memcached;
//    private $redis;
//    private $database;
//
//    private function makeMemcacheConnections(){
//
//    }


    public static function getMemcacheInfo()
    {
        $servers = [];
        $servers[] = new MemcacheServerInfo(MEMCACHE_HOST, MEMCACHE_PORT, MEMCACHE_TIMEOUT);

        return $servers;
    }

    public static function getRedisInfo(){
        return new RedisServerInfo(REDIS_HOST, REDIS_PORT);
    }

}