<?php
/**
 * Created by PhpStorm.
 * User: 이창선
 * Date: 2018-01-16
 * Time: 오후 3:10
 */

namespace GameServer\Config;

class DatabaseServerInfo{
    private $host;
    private $name;
    private $charSet;
    private $userName;
    private $password;

    /**
     * DatabaseServerInfo constructor.
     * @param $host
     * @param $name
     * @param $charSet
     * @param $userName
     * @param $password
     */
    public function __construct($host, $name, $charSet, $userName, $password)
    {
        $this->host = $host;
        $this->name = $name;
        $this->charSet = $charSet;
        $this->userName = $userName;
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCharSet()
    {
        return $this->charSet;
    }

    /**
     * @param mixed $charSet
     */
    public function setCharSet($charSet)
    {
        $this->charSet = $charSet;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

}