<?php
/**
 * Created by PhpStorm.
 * User: 이창선
 * Date: 2018-01-16
 * Time: 오후 2:50
 */

namespace GameServer\Config;

class MemcacheServerInfo
{
    private $host;
    private $port;
    private $timeout;

    /**
     * MemcachedServerInfo constructor.
     * @param $host
     * @param $port
     * @param $timeout
     */
    public function __construct($host, $port, $timeout)
    {
        $this->host = $host;
        $this->port = $port;
        $this->timeout = $timeout;
    }

    /**
     * @return mixed
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param mixed $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return mixed
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param mixed $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }

    /**
     * @return mixed
     */
    public function getTimeout()
    {
        return $this->timeout;
    }

    /**
     * @param mixed $timeout
     */
    public function setTimeout($timeout)
    {
        $this->timeout = $timeout;
    }

}

