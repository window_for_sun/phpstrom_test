<?php
/**
 * Created by PhpStorm.
 * User: 이창선
 * Date: 2018-01-16
 * Time: 오후 4:21
 */

namespace GameServer\Controller;

use GameServer\Config\ConfigInfo;

class RedisController{
    private static $instance = null;

    private $redis = null;

    private function __construct(){
        $redisInfo = ConfigInfo::getRedisInfo();
        $this->redis = new \Redis();

        if($this->redis->pconnect($redisInfo->getHost(), $redisInfo->getPort()) === false){
            throw new SystemException("failure connect redis");
        }
    }

    public function __destruct(){
        self::$instance = null;
    }

    public function setData($key, $score, $userUID){

    }

    public function getData($key, $userUID){
        return (int)($this->redis->zScore($key, $userUID));
    }

    public function deleteData($key, $userUID){
        $this->redis->zDelete($key, $userUID);
    }

    public function getAllRank($key){
        return $this->redis->zRevRangeByScore($key, '+inf', 0, array('withscores'=>true));
    }

    public function getRank($key, $userUID){
        return $this->redis->zRevRank($key, $userUID);
    }


}