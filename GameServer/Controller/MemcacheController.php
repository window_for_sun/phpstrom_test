<?php
/**
 * Created by PhpStorm.
 * User: 이창선
 * Date: 2018-01-16
 * Time: 오후 3:24
 */

namespace GameServer\Controller;

use GameServer\Config\ConfigInfo;
use GameServer\Config\MemcacheServerInfo;

class MemcacheController
{
    private static $instance = null;
    private $memcache;

    /**
     * MemcacheController constructor.
     */
    private function __construct()
    {
        $servers = ConfigInfo::getMemcacheInfo();

        $this->memcache = new Memcache();

        if($this->memcache === null || $this->memcache instanceof \Memcache === false){
            throw new SystemException('fail create memcache');
        }

        foreach($servers as $server){
            if($server instanceof MemcacheServerInfo === false){
                continue;
            }

            if($this->memcache->addServer($server->getHost(), $server->getPort(), true) === false){
                throw new SystemException("failure memcache add server");
            }

            if($this->memcache->pconnect($server->getHost(), $server->getPort(), $server->getTimeout()) === false){
                throw new SystemException("failure memcache connect");
            }
        }

        self::$instance = $this;
    }

    public function __destruct(){
        $this->memcache = null;
        self::$instance = null;
    }

    public static function getInstance(){
        if(self::$instance === null){
            new MemcacheController();
        }

        return self::$instance;
    }

    public function setData($key, $var, $expire = 0){
        $ret = $this->memcache->set($key, $var, false, $expire);

        if($ret === false){
            throw new SystemException("failure memcache set data");
        }
    }

    public function getData($key){
        $ret = $this->memcache->get($key);

//        if($ret === false){
//
//        }

        return $ret;
    }

    public function deleteData($key){
        $ret = $this->memcache->delete($key);

        return $ret;
    }
}