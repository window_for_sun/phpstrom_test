<?php
/**
 * Created by PhpStorm.
 * User: 이창선
 * Date: 2018-01-15
 * Time: 오후 7:09
 */


$redis = new Redis();
$redis->connect('localhost', 6379);
$redis->select(0);

class Test{
    private $a;
    private $b;

    public function Test($a, $b){
        $this->a = $a;
        $this->b = $b;
    }

    public function getA(){
        return $this->a;
    }

    public function getB(){
        return $this->b;
    }
}

$test = new Test(123, '123');
$tmp = serialize($test);
$redis->set('obj', $tmp);
$t = $redis->get('obj');
$recover = unserialize($t);

print_r($recover);

$redis->close();